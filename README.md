## Project Chrono

Testing Chrono 6.0.0 installed on Windows 10

https://projectchrono.org/

# Tested
Vehicle simulations
Deformable terrain simulations
Driver simulations
Pathfollowing simulations

# Issues
If the Chrono installed via .exe, only Release projects can be made.
Missing Nvidia CUB, Multicore simulations freezing, issue with Nvidia Thrust
Can't find properly the data dir, symbolic link necessary in the build folder
